# frozen_string_literal: true

# This file ensure the presence of minimum data on new deployed environments.
# Make sure to keep it up to date to be able to rapidly review the changes.
#
# Thanks to the environment variable `DB_INITIALIZE`, the task db:seed
# is called at each new deloyment.
# For all already existing deployment (production and review alike) only
# `DB_MIGRATE` will be lunched. It needs to be fast (less than few minutes),
# then do not create indexes too late, else you'll have to update the
# deployment script to expect a long migration.

# Anyway, it could be a good idea to be able to update already existing entity
# when adding new fields. Here we allow to update users based on their email,
# this is not that good, because it may endup making known users with high
# rights over your application. Maybe for updating existing users, you should
# use another way.

# Here we lower the log level to not see all the actions made on the database
# during seeding and being able to see some informational logs we may put here.
old_level = Rails.logger.level
Rails.logger.level = :INFO

Rails.logger.info 'Creating/updating new users:'
[
  { email: 'user1@example.com', password: 'Abc0123456789!', password_confirmation: 'Abc0123456789!' },
  { email: 'user2@example.com', password: 'Bcd0123456789!', password_confirmation: 'Bcd0123456789!' }
].each do |user_h|
  Rails.logger.info "  - email : #{user_h[:email]},"
  user = User.find_by(email: user_h[:email])
  if user.nil?
    user = User.create user_h
    Rails.logger.info "    Created with id: #{user.id},"
  else
    Rails.logger.info "    User was already created: #{user.id},"
  end
  user.confirm
  Rails.logger.info '    User has been confirmed and can connect now.'
end

## ---- Ne pas rajouter de seed après cette ligne ---
## ---- Don't add seed data after this line ---
## We reserve this section to go back to normal usage of the seed behavior.
# Reset Log level to normal
Rails.logger.level = old_level

# frozen_string_literal: true

require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module RailsBaseApplication
  # The main application class.
  #
  # Here is the configuration of the Rails app.
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.1

    # Please, add to the `ignore` list any other `lib` subdirectories that do
    # not contain `.rb` files, or that should not be reloaded or eager loaded.
    # Common ones are `templates`, `generators`, or `middleware`, for example.
    config.autoload_lib(ignore: %w[assets tasks])

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    config.time_zone = 'UTC'
    # config.eager_load_paths << Rails.root.join("extras")

    # Use Rspec for testing
    config.generators.test_framework = :rspec

    # Locales definition
    # Permitted locales available for the application
    config.i18n.available_locales = %i[fr en]
    config.i18n.default_locale = :fr

    # Compile assets
    config.sass.preferred_syntax = :sass
    config.sass.line_comments = true
    config.sass.cache = false

    # Automatically configure the hostname for the workers and mailer which
    # doesn't have a context.
    # It is based on a deployment made by GITLAB CI.
    host_uri = URI.parse(ENV.fetch('GITLAB_ENVIRONMENT_URL', 'http://[::1]'))
    # Mise en place d'un hote par défault pour les envoies d'email.
    config.action_mailer.default_url_options = {
      host: host_uri.host, port: host_uri.port
    }
  end
end

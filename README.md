# Références

## Introduction

Application permettant le référencement des performances de support de stockage vendu par Liant.shop.

Il sera à terme possible de rajouter vos propres tests.

## État du projet

[![pipeline status](https://gitlab.com/liant-sasu/references/badges/main/pipeline.svg?ignore_skipped=true)](https://gitlab.com/liant-sasu/references/-/commits/main)
[![coverage report](https://gitlab.com/liant-sasu/references/badges/main/coverage.svg)](https://gitlab.com/liant-sasu/references/-/commits/main)
[![Latest Release](https://gitlab.com/liant-sasu/references/-/badges/release.svg)](https://gitlab.com/liant-sasu/references/-/releases)

### Déploiement

Le déploiement a lieu dans le cloud Liant à l'adresse `https://references.liant.cloud`
pour la release principale et sur un lien créé à partir du nom de la branche comme
ceci : https://\<branch\>.references.liant.cloud. Le mot \<branche\> vient de la variable
prédéfinie par GitLab **CI_ENVIRONMENT_SLUG** qui lui même est configuré pour contenir
`review/$CI_COMMIT_REF_SLUG` tronqué à 24 caractères avec un suffix aléatoire de 6
caractères. **CI_COMMIT_REF_SLUG** est quant à lui construit à partir du nom de la branche ou tag.

Un exemple est le premier environnement créé qui était à l'adresse :
`https://review-feature-me-981wv8.references.liant.cloud/`

## Développement

### Dépendances

- Postgres (`libpg`)

### MacOS

Sur MacOS, il est nécessaire d'installer avec `brew` par exemple la bibliothèque libpg, puis configurer bundle pour l'utiliser correctement.

    brew install libpg
    bundle config build.pg --with-pg-config=/opt/homebrew/opt/libpq/bin/pg_config
    bundle

## Contribution et utilisation

Le projet est sous license GNU GPLv3 et vous pouvez donc vous en inspirer ou même le récupérer comme bon vous semble.

Seul les éléments graphiques comme les logos de Liant sont des marques déposées. Utilisez donc le votre pour votre projet.

Vous pouvez proposer des modifications en bifurquant le projet et en remplissant des tickets.

Mise à part ce README pour le moment, tout le code et les commentaires doivent être rédigés en anglais.

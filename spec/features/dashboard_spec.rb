# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Dasboard' do
  context 'when visitor is not signed in' do
    it 'displays sign in page' do
      visit dashboard_path

      expect(page).to have_current_path(new_user_session_path)
    end
  end

  context 'when visitor is signed in' do
    let(:user) { create(:confirmed_user) }

    before do
      visit new_user_session_path

      fill_in :user_email, with: user.email
      fill_in :user_password, with: 'Ab0123456789'
      find('form.new_user input.btn').click
    end

    it 'displays without redirecting' do
      visit dashboard_path

      expect(page).to have_current_path(dashboard_path)
    end
  end
end
